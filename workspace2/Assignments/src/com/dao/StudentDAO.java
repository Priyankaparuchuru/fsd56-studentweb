package com.dao;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.db.DbConnection;
import com.dto.Student;


public class StudentDAO {
	

	public Student stdLogin(String email, String password) {

		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		String loginQuery = "Select * from student where email=? and password=?";


		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, email);
			pst.setString(2, password);
			rs = pst.executeQuery();

			if (rs.next()) {
				Student std = new Student();
				std.setid(rs.getInt(1));
				std.setname(rs.getString(2));
				std.setgender(rs.getString(3));
				std.setemail(rs.getString(4));
				std.setpassword(rs.getString(5));
				std.setdepartment(rs.getString(6));
				return std;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}


		return null;
	}
	public List<Student> getAllStudents() {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;		
		List<Student> stdList = null;

		String selectQuery = "Select * from student";


		try {
			pst = con.prepareStatement(selectQuery);
			rs = pst.executeQuery();

			stdList = new ArrayList<Student>();

			while (rs.next()) {
				Student std = new Student();

				std.setid(rs.getInt(1));
				std.setname(rs.getString(2));
				std.setgender(rs.getString(3));
				std.setemail(rs.getString(4));
				std.setpassword(rs.getString(5));
				std.setdepartment(rs.getString(6));

				stdList.add(std);
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return stdList;
	}

	public int registerStudent(Student std) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;

		String insertQuery = "insert into student " + 
				"(name, gender, email, password, department) values (?, ?, ?, ?, ?)";

		try {
			pst = con.prepareStatement(insertQuery);

			pst.setString(1, std.getname());
			pst.setString(2, std.getgender());
			pst.setString(3, std.getemail());
			pst.setString(4, std.getpassword());
			pst.setString(5, std.getdepartment());

			return pst.executeUpdate();

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return 0;
	}

	public Student getStdById(int id) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		String selectQuery = "Select * from student where id=?";


		try {
			pst = con.prepareStatement(selectQuery);
			pst.setInt(1, id);
			rs = pst.executeQuery();

			if (rs.next()) {

				Student std = new Student();

				std.setid(rs.getInt(1));
				std.setname(rs.getString(2));
				std.setgender(rs.getString(3));
				std.setemail(rs.getString(4));
				std.setpassword(rs.getString(5));
				std.setdepartment(rs.getString(6));

				return std;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		return null;
	}
	public Student getStudentById(int id) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;

		String selectQuery = "Select * from student where id=?";


		try {
			pst = con.prepareStatement(selectQuery);
			pst.setInt(1, id);
			rs = pst.executeQuery();

			if (rs.next()) {

				Student std = new Student();

				std.setid(rs.getInt(1));
				std.setname(rs.getString(2));
				std.setgender(rs.getString(3));
				std.setemail(rs.getString(4));
				std.setpassword(rs.getString(5));
				std.setdepartment(rs.getString(6));

				return std;
			}

		} catch (SQLException e) {
			e.printStackTrace();
		}

		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return null;
	}
	public int updateStudent(Student std) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String updateQuery = "update student set name=?, gender=?, email=?, password=?, department=? where id=?";
		
		try {
			pst = con.prepareStatement(updateQuery);
			
			pst.setString(1, std.getname());
			pst.setString(2, std.getgender());
			pst.setString(3, std.getemail());
			pst.setString(4, std.getpassword());
			pst.setString(5, std.getdepartment());
			pst.setInt(6, std.getid());
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}
	public int deleteStudent(int id) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String deleteQuery = "delete from student where id=?";
		
		
		try {
			pst = con.prepareStatement(deleteQuery);
			pst.setInt(1, id);
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}

		return 0;
	}

}
